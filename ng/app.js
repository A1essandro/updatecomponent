function TodoCtrl($scope, $http, $sce) {

    $scope.log = '';
    $scope.bhide = 0;
    $scope.logShow = 0;
    
    $scope.startUpdate = function () {
        $scope.bhide = 1;
        $http.get('/update.php').success(function (data) {
            $scope.bhide = 0;
            $http.get('/log/' + data).success(function (data) {
                $scope.log = $sce.trustAsHtml(data);
            });
        });

    }


}