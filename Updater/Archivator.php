<?php

namespace Updater;

/**
 * Description of Archivator
 *
 * @author Alexander Yermakov
 */
class Archivator extends \ZipArchive
{

    const EXTENSION = '.zip';

    /**
     * Объект главного класса апдейтера
     * @var Updater\Updater
     */
    private $updater;

    /**
     * Конструктор
     * @param Updater $updater
     */
    public function __construct($updater)
    {
        $this->updater = $updater;
        $this->updater->logger->add('Создание архиватора');
    }

    /**
     * Распаковка архива Update::zipFile в Updater::tempDir
     * @return boolean
     * @throws \Exception
     */
    public function unpack()
    {
        $this->updater->logger->add('Открытие архива ' . $this->updater->zipFile);
        $res = $this->open($this->updater->zipFile);

        if (!$res)
        {
            $this->updater->logger->add('Ошибка при открытии архива ' . $this->updater->zipFile);
            throw new \Exception('Ошибка при открытии архива ' . $this->updater->zipFile);
        }

        $this->updater->logger->add('Распаковка архива в ' . $this->updater->tempDir);
        if (!$this->extractTo($this->updater->tempDir))
        {
            $this->updater->logger->add('Ошибка распаковки');
            throw new \Exception('Ошибка распаковки');
        }
        chmod($this->updater->tempDir, 0777);
        $folderName = str_replace('/', '', $this->getNameIndex(0));

        $this->close();
        $this->updater->logger->add('Распаковка папки ' . $folderName . ' завершена');

        return $folderName;
    }

    /**
     * Упаковка архива
     * @param type $targetPath
     * @param type $folder
     * @param type $isOpen
     * @return boolean
     */
    function pack($targetPath, $folder, $isOpen = false)
    {
        //если файл не открыт - [создаем и]открываем
        if (!$isOpen)
        {
            if (!is_dir($folder))
                return false;

            if (!is_file($targetPath))
            {
                $this->updater->logger->add('Создание архива ' . $targetPath);
                $this->updater->fileManager->createFile($targetPath);
            }
            $this->open($targetPath);
        }

        //Рекурсивное добавление файлов в архив
        $handle = opendir($folder);
        while (false !== $f = readdir($handle))
        {
            if ($f != '.' && $f != '..')
            {
                $filePath = $folder . DIRECTORY_SEPARATOR . $f;
                $loc = str_replace($this->updater->unpackPath . DIRECTORY_SEPARATOR, '', $filePath);
                if (is_file($filePath))
                {
                    $this->addFile($filePath, $loc);
                } elseif (is_dir($filePath))
                {
                    $this->addEmptyDir($filePath);
                    $this->pack($targetPath, $filePath, true);
                }
                $this->updater->logger->add('Добавлен в архив ' . $filePath);
            }
        }
        closedir($handle);

        //Если это корень рекурсии - закрываем архив и возвращаем его объем
        if (!$isOpen)
        {
            $this->close();
            return filesize($targetPath);
        }
    }

}
