<?php

namespace Updater;

/**
 * Description of Logger
 *
 * @author Alexander Yermakov
 */
class Logger
{

    const LOG_DIR = 'log';

    /**
     * Путь к текущему файлу логов
     * @var string
     */
    private $file;

    /**
     * Поток для записи логов
     * @var resource
     */
    private $fileStream;

    /**
     * Объект главного класса апдейтера
     * @var Updater\Updater
     */
    private $updater;

    /**
     * Корень файлов логов
     * @var string 
     */
    private $rootDir;

    /**
     * Конструктор
     * @param Updater\Updater $updater
     */
    public function __construct($updater)
    {
        $this->updater = $updater;
        $this->rootDir = $this->updater->rootPath . DIRECTORY_SEPARATOR . self::LOG_DIR;
        $fileName = time() . '.txt';
        $this->file = $this->rootDir . DIRECTORY_SEPARATOR . $fileName;
        echo $fileName;
        if (!is_dir($this->rootDir))
            mkdir($this->rootDir);
        $this->fileStream = fopen($this->file, 'w');
    }

    /**
     * Добавляет строку лога в файл
     * @param string $message сообщение лога
     * @return int|bool
     */
    public function add($message)
    {
        $line = date('H:i:s') . ' ' . $message;
        return fwrite($this->fileStream, str_replace("\r\n", '', $line) . " <br/>\r\n");
    }

    /**
     * При удалении объекта - закрывает поток
     */
    public function __destruct()
    {
        fclose($this->fileStream);
    }

}
