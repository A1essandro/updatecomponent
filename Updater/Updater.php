<?php

namespace Updater;

/**
 * Description of Updater
 *
 * @author Alexander Yermakov
 */
class Updater
{

    const TEMP_DIR = 'temp';
    const BACKUP_DIR = 'backups';

    /**
     * Загрузчик
     * @var Updater\Downloader
     */
    private $downloader;

    /**
     * Архиватор
     * @var Updater\Archivator
     */
    private $archivator;

    /**
     * Корневой каталог
     * @var string
     */
    public $rootPath;

    /**
     * Путь на удаленный сервер к файлу для загрузки
     * @var string 
     */
    public $downloadedPath = null;

    /**
     * Локальный путь к скачиваемому файлу
     * @var string 
     */
    public $zipFile;

    /**
     * Путь к временной директории
     * @var string 
     */
    public $tempDir;

    /**
     * Хэш проверки обновлений
     * @var string 
     */
    public $ETag = '';

    /**
     *
     * @var Updater\Logger 
     */
    public $logger;
    public $fileManager;
    public $unpackPath;

    function __construct($downloadedPath, $unpackPath = null, $rootPath = __DIR__)
    {
        $this->downloadedPath = $downloadedPath;
        $this->rootPath = $rootPath;

        $this->tempDir = $this->rootPath . DIRECTORY_SEPARATOR . self::TEMP_DIR;
        $this->zipFile = $this->tempDir . DIRECTORY_SEPARATOR . uniqid() . Archivator::EXTENSION;
        $this->unpackPath = $unpackPath? : $this->tempDir;

        $this->logger = new Logger($this);
        $this->fileManager = new FileManager($this);
    }

    /**
     * Загрузка файлов
     */
    public function download()
    {
        $this->downloader = new Downloader($this);
        $this->downloader->download();

        return $this;
    }

    /**
     * Распаковка и замена файлов
     */
    public function modify()
    {
        $this->initArchivator();
        $folder = $this->archivator->unpack();

        $this->logger->add('Старт обновления рабочих файлов');
        $this->fileManager->moveDir($this->tempDir . DIRECTORY_SEPARATOR . $folder, $this->unpackPath);
        $this->logger->add('Обновление рабочих файлов завершено');
        $this->setETag();

        return $this;
    }

    /**
     * Создание резервной копии
     */
    public function backUp()
    {
        $this->initArchivator();
        $this->logger->add('Начало резервного копирования');

        $path = $this->rootPath . DIRECTORY_SEPARATOR . self::BACKUP_DIR . DIRECTORY_SEPARATOR . time() . '.zip';
        $res = $this->archivator->pack($path, $this->unpackPath);
        if (!$res)
            $this->logger->add('Ошибка резервного копирования');
        else
            $this->logger->add('Резервное копирование завершено. ' . $res . ' байт');

        return $this;
    }

    /**
     * Инициализация архиватора
     */
    private function initArchivator()
    {
        if (empty($this->archivator))
            $this->archivator = new Archivator($this);

        return $this;
    }

    /**
     * Окончание обновления
     */
    function __destruct()
    {
        $this->logger->add('Удаление временной директории');
        $this->fileManager->rmdir($this->tempDir);
        $this->logger->add('Обновление завершено');
    }

    /**
     * Проверка наличия обновления
     * @return boolean
     */
    function checkUpdates()
    {
        $curl = curl_init();
        $this->logger->add('Проверка наличия обновлений');

        if ($curl)
        {
            $headerMatches = array();
            curl_setopt($curl, CURLOPT_URL, $this->downloadedPath);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            curl_setopt($curl, CURLOPT_HEADER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $ETag = uniqid();
            if (preg_match('/ETag:\s*([\da-z\"\-]*)/i', curl_exec($curl), $headerMatches))
                $ETag = $headerMatches[1];

            curl_close($curl);

            if ($ETag != $this->getETag())
            {
                $this->logger->add('Есть обновления (' . $ETag . ' != ' . $this->getETag() . ')');
                return true;
            }
        }

        $this->logger->add('Обновлений не найдено');
        return false;
    }

    /**
     * Сохранение ETag в файл для проверки обновлений
     */
    public function setETag()
    {
        if (!$this->ETag)
            return;

        $ETagPath = $this->rootPath . DIRECTORY_SEPARATOR . 'ETag';
        $handle = $this->fileManager->fopen($ETagPath, 'w+');
        fwrite($handle, $this->ETag);
        $this->logger->add('Обновление установлено под идентификатором ' . $this->ETag);
        $this->fileManager->fclose($handle);
    }

    /**
     * Получение идентификатора обновления
     */
    public function getETag()
    {
        $ETagPath = $this->rootPath . DIRECTORY_SEPARATOR . 'ETag';

        if (!is_file($ETagPath))
            return rand(1, 99);

        return file_get_contents($ETagPath);
    }

}
