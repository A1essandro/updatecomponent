<?php

namespace Updater;

/**
 * Description of Downloader
 *
 * @author Alexander Yermakov
 */
class Downloader
{

    const TIMEOUT_SOCKET = 15;

    /**
     * Результат выполнения функции curl_init()
     * @var resource 
     */
    private $curl;

    /**
     * Количество полученных данных в байтах
     * @var int 
     */
    private $currentContentLength = 0;

    /**
     * Объект главного класса апдейтера
     * @var Updater\Updater
     */
    private $updater;

    /**
     * поток для загрузки файла
     * @var resource
     */
    private $stream;

    /**
     * 
     * @param Updater $updater
     * @throws \Exception
     */
    function __construct($updater)
    {
        $this->updater = $updater;
        $this->updater->logger->add('Создание загрузчика');
        $this->updater->logger->add('Проверка инициализации curl');
        if (!($this->curl = curl_init()))
        {
            $this->updater->logger->add('curl не включен!');
            throw new \Exception('err_curlinit');
        }
        $this->updater->logger->add('Инициализация curl прошла успешно');

        if (curl_errno($this->curl) != 0)
        {
            $this->updater->logger->add('Ошибка curl; ' . 'err_curlinit' . curl_errno($this->curl) . ' ' . curl_error($this->curl));
            throw new \Exception('err_curlinit' . curl_errno($this->curl) . ' ' . curl_error($this->curl));
        }
    }

    /**
     * Проверка возможности загрузки с помощью curl
     * @return boolean
     */
    private function isAvailable()
    {
        $this->updater->logger->add('Проверка возможности загрузки с помощью curl');
        return extension_loaded('curl') &&
                function_exists('curl_init') &&
                preg_match('/https?:\/\//', $this->updater->downloadedPath);
    }

    /**
     * Попытка загрузки файла
     * @throws \Exception
     */
    public function download()
    {
        $this->stream = $this->updater->fileManager->fopen($this->updater->zipFile, 'wp');
        if (!$this->isAvailable())
        {
            $this->updater->logger->add('Загрузка с помощью curl невозможна');
            throw new \Exception('CURL is not available');
        }
        $this->updater->logger->add('Загрузка с помощью curl возможна');

        $curl_options = array(
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => self::TIMEOUT_SOCKET * 60,
            CURLOPT_CONNECTTIMEOUT => self::TIMEOUT_SOCKET,
            CURLE_OPERATION_TIMEOUTED => self::TIMEOUT_SOCKET * 60,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_WRITEFUNCTION => array($this, 'curlWriteHandler'),
            CURLOPT_HEADERFUNCTION => array($this, 'curlHeaderHandler'),
            CURLOPT_URL => $this->updater->downloadedPath,
                //TODO на ряде хостингов curl работает только через прокси, который необходимо указать в настройках
        );

        foreach ($curl_options as $param => $option)
            curl_setopt($this->curl, $param, $option);

        curl_exec($this->curl);

        $errno = curl_errno($this->curl);
        if ($errno)
        {
            $this->updater->logger->add('Ошибка curl; ' . "Curl error: {$errno}# " . curl_error($this->curl) . " at [{$this->updater->downloadedPath}]");
            throw new \Exception("Curl error: {$errno}# " . curl_error($this->curl) . " at [{$this->updater->downloadedPath}]");
        }

        $status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        if ($status != 200)
        {
            $this->updater->logger->add("curl: Неверный ответ сервера {$this->updater->downloadedPath}");
            throw new \Exception("Неверный ответ сервера {$this->updater->downloadedPath}", $status);
        }

        curl_close($this->curl);
        $this->updater->fileManager->fclose($this->stream);
    }

    /**
     * обработчик чтения заголовков для curl
     *
     * @param $ch
     * @param $header
     * @return int
     */
    private function curlHeaderHandler($ch, $header)
    {
        $this->updater->logger->add('curl: чтение заголовка: ' . $header);
        $header_matches = null;

        if (preg_match('/content-length:\s*(\d+)/i', $header, $header_matches))
            $this->content_length = intval($header_matches[1]);
        if (preg_match('/ETag:\s*([\da-z\"\-]*)/i', $header, $header_matches))
            $this->updater->ETag = $header_matches[1];
        return strlen($header);
    }

    /**
     * обработчик записи в файл для curl
     * 
     * @param $ch
     * @param $chunk
     * @return int
     */
    private function curlWriteHandler($ch, $chunk)
    {
        $size = 0;
        if ($this->stream && is_resource($this->stream))
        {
            $size = fwrite($this->stream, $chunk);
            $this->currentContentLength += $size;
            $this->updater->logger->add('curl: запись файла (поток [' . $this->stream . ']): ' . $this->currentContentLength . ' байт');
        } else
        {
            $this->updater->logger->add('curl: ошибка сохранения файла на сервере');
            throw new \Exception('Ошибка сохранения файла на сервере');
        }
        return $size;
    }

}
