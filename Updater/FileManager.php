<?php

namespace Updater;

/**
 * Description of FileManager
 *
 * @author Alexander Yermakov
 */
class FileManager
{

    /**
     * Объект главного класса апдейтера
     * @var Updater\Updater
     */
    private $updater;

    /**
     * Конструктор
     * @param Updater $updater
     */
    public function __construct($updater)
    {
        $this->updater = $updater;
        $this->updater->logger->add('Создание файлового менеджера');
    }

    /**
     * Возвращает ресурс (результат fopen(file))
     * @return resource
     */
    public function fclose($handle)
    {
        $this->updater->logger->add('Закрытие потока [' . $handle . ']');
        return fclose($handle);
    }

    /**
     * "Настойчивое" открытие файла
     * на случай если ресурс занят другим процессом или директория еще не создана
     * @param $filename
     * @param $mode
     * @param $retry
     * @return resource
     */
    public function fopen($filename, $mode, $retry = 5)
    {
        if (!file_exists($filename))
        {
            $this->mkdir(dirname($filename));
            chmod(dirname($filename), 0755);
        }

        while (!($fp = fopen($filename, $mode)))
            if (--$retry > 0)
                sleep(1);
            else
                break;

        $this->updater->logger->add('Создание потока [' . $fp . '] файла ' . $filename);

        return $fp;
    }

    /**
     * Создание директорий с дополнительными проверками на права записи
     *
     * @param $targetPath
     * @param $mode
     * @return void
     */
    private function mkdir($targetPath, $mode = 0777)
    {
        $this->updater->logger->add('Создание директории ' . $targetPath);
        if (!file_exists($targetPath))
            if (!mkdir($targetPath, $mode & 0777, true))
                throw new \Exception("не могу создать директорию {$targetPath}");
            elseif (!is_dir($targetPath))
                throw new \Exception("Не могу создать директорию {$targetPath}, так как есть файл с таким изменем");
            elseif (!is_writable($targetPath))
                throw new \Exception("{$targetPath} должна быть доступна по записи. Установите необходимые права доступа.");
    }

    /**
     * Создание файла
     * @param type $targetPath путь
     * @param type $chmod права доступа
     */
    public function createFile($targetPath, $chmod = 0777)
    {
        $fp = $this->fopen($targetPath, 'w');
        chmod($targetPath, $chmod);
        fclose($fp);
    }

    /**
     * Рекурсивное удаление папок
     * @param string $dir Путь к папке
     * @return null
     */
    public function rmdir($dir)
    {
        if (!is_dir($dir))
            return;

        $this->updater->logger->add('Очистка директории ' . $dir);

        $objects = scandir($dir);
        foreach ($objects as $object)
            if ($object !== "." && $object !== "..")
            {
                if (filetype($dir . DIRECTORY_SEPARATOR . $object) == "dir")
                    $this->rmdir($dir . DIRECTORY_SEPARATOR . $object);
                else
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
            }
        reset($objects);

        $this->updater->logger->add('Удаление директории ' . $dir);
        rmdir($dir);
    }

    /**
     * Рекурсивное перемещение файлов
     * @param type $from
     * @param type $to
     */
    public function moveDir($from, $to)
    {
        $handle = opendir($from);
        if (!is_dir($to))
            mkdir($to, 0755);
        while ($file = readdir($handle))
            if (($file != ".") and ( $file != ".."))
            {
                $srcm = $from . DIRECTORY_SEPARATOR . $file;
                $dstm = $to . DIRECTORY_SEPARATOR . $file;
                $srcm = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $srcm);
                $dstm = str_replace(DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $dstm);
                $this->updater->logger->add('Перемещение ' . $srcm . ' --> ' . $dstm);
                if (is_dir($srcm))
                {
                    $this->moveDir($srcm, $dstm);
                } else
                {
                    copy($srcm, $dstm);
                    unlink($srcm);
                }
            }

        closedir($handle);
        rmdir($from);
    }

}
