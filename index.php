<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title>Admin page - Questions</title>
    </head>
    <body style="height: 1000px;" ng-app ng-controller="TodoCtrl">
        <div>
            <h2>Todo</h2>
            <div >
                <form ng-submit="startUpdate()" ng-hide="bhide">
                    <input class="btn-primary" type="submit" value="Update">
                </form>
                <div ng-show="log">
                    <div ng-show="logShow">
                        <span style="color:green;" ng-click="logShow = 0">Скрыть лог</span>
                        <div ng-bind-html="log">
                            {{log}}
                        </div>
                    </div>
                    <div ng-hide="logShow">
                        <span style="color:green" ng-click="logShow = 1">Показать лог</span>
                    </div>
                </div>
            </div>
        </div>
        <script src="/ng/lib/angular.js"></script>
        <script src="/ng/app.js"></script>
    </body>
</html>