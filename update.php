<?php

header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);

function __autoload($className)
{
    include $className . '.php';
}

try {
    $updater = new Updater\Updater('https://bitbucket.org/A1essandro/updatecomponent/get/HEAD.zip', __DIR__ . DIRECTORY_SEPARATOR . 'unpack', __DIR__);
    if ($updater->checkUpdates())
        $updater->download()->backUp()->modify();
} catch (Exception $ex) {
    echo $ex->getMessage();
}


